package tn.esprit.perso.labo.persistence;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

@Entity
// @DiscriminatorColumn(name="grade")
// @Inheritance(strategy=InheritanceType.SINGLE_TABLE)

// @Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
@Inheritance(strategy = InheritanceType.JOINED)
public class Employe {

	private String login;
	private String password;
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	private String cin;
	private String nom;
	private String prenom;
	private Labo labo;
	private Adresse adrese;

	public Employe() {
		// TODO Auto-generated constructor stub
	}

	public Employe(String cin, String nom, String prenom) {
		this.cin = cin;
		this.nom = nom;
		this.prenom = prenom;
	}

	@Id
	public String getCin() {
		return cin;
	}

	public void setCin(String cin) {
		this.cin = cin;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	@ManyToOne
	public Labo getLabo() {
		return labo;
	}

	public void setLabo(Labo labo) {
		this.labo = labo;
	}

	@Embedded
	public Adresse getAdrese() {
		return adrese;
	}

	public void setAdrese(Adresse adrese) {
		this.adrese = adrese;
	}

}
