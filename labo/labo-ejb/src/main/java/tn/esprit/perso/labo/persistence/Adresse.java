package tn.esprit.perso.labo.persistence;

import javax.persistence.Embeddable;

@Embeddable
public class Adresse {

	private String cite;
	private String ville;
	private String balass;
	private int codePostal;
	
	public String getCite() {
		return cite;
	}
	public void setCite(String cite) {
		this.cite = cite;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getBalass() {
		return balass;
	}
	public void setBalass(String balass) {
		this.balass = balass;
	}
	public int getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(int codePostal) {
		this.codePostal = codePostal;
	}
	
	
}
