package tn.esprit.perso.labo.persistence;

import javax.persistence.Entity;

@Entity
public class Admin extends Employe{
	
	private int niveau;

	public int getNiveau() {
		return niveau;
	}

	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}

}
